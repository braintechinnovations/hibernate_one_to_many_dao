package corso.lez18.HibernateOneToManyDAO.models.crud;

import java.util.List;

import org.hibernate.Session;

import corso.lez18.HibernateOneToManyDAO.models.Carta;
import corso.lez18.HibernateOneToManyDAO.models.Persona;
import corso.lez18.HibernateOneToManyDAO.models.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona>{

	@Override
	public void insert(Persona t) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Persona findById(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			Persona temp = sessione.get(Persona.class, id);
			
			sessione.getTransaction().commit(); 

			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
		return null;
		
	}


	@Override
	public List<Persona> findAll() {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			List<Persona> elenco = sessione.createQuery("FROM Persona").list();
			
			sessione.getTransaction().commit(); 
			
			return elenco;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();

			Persona temp = sessione.load(Persona.class, id);
			sessione.delete(temp);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(Persona t) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.delete(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean update(Persona t) {
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.update(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
