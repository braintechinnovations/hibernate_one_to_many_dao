package corso.lez18.HibernateOneToManyDAO;

import corso.lez18.HibernateOneToManyDAO.models.Carta;
import corso.lez18.HibernateOneToManyDAO.models.Persona;
import corso.lez18.HibernateOneToManyDAO.models.crud.CartaDAO;
import corso.lez18.HibernateOneToManyDAO.models.crud.PersonaDAO;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	PersonaDAO perDao = new PersonaDAO();
    	CartaDAO carDao = new CartaDAO();
    	
    	//INSERIMENTO
    	
//    	Persona mar = new Persona("Maria", "Blabla", "MARBLA");
//    	
//    	Carta carUno = new Carta("Tigotà", "TIG123456");
//    	carUno.setProprietario(mar);
//    	
//    	Carta carDue = new Carta("Tuodì", "TD123456");
//    	carDue.setProprietario(mar);
//    	
//    	perDao.insert(mar);
//    	carDao.insert(carUno);
//    	carDao.insert(carDue);
//    	
//    	//TODO: Controlli di inserimento
    	
    	System.out.println(carDao.findById(2).stampaDettaglioCarta());
    	

    }
}
